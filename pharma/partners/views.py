from django.shortcuts import render
from django.http import HttpResponse

def contact_form(request):
    return render(request, 'partners/contact_form.html')