from django.urls import path
from . import views

urlpatterns = [
    path('', views.shop, name='shop'),
    path('order_storage/', views.order_from_storage, name='shop_order_storage'),
]