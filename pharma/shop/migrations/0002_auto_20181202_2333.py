# Generated by Django 2.1.3 on 2018-12-02 21:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='shop',
            name='pharma_rice',
            field=models.FloatField(default=6),
        ),
        migrations.AddField(
            model_name='shop',
            name='purchase_price',
            field=models.FloatField(default=5),
        ),
    ]
