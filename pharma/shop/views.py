from django.shortcuts import render, redirect
from django.http import HttpResponse
from shop.models import Shop
from .forms import OrderFromStorage
from django.contrib import messages


def shop(request):
    data = Shop.objects.all()
    context = {'data': data}
    return render(request, 'shop/shop.html', context)

def order_from_storage(request):
    if request.method == 'POST':
        form = OrderFromStorage(request.POST, request.FILES)
        import pdb;pdb.set_trace()
        if form.is_valid():
            name = form.cleaned_data.get('name')
            messages.success(request, f'Замовлення {name} успішно створене!')
            form.save()
            return redirect('home_thank_you')
    else:
        form = OrderFromStorage()
    return render(request, 'shop/order_from_storage.html', {'form': form})

def customer_order(request):
    pass

