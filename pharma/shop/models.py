from django.db import models
from django.contrib.auth.models import User
from decimal import Decimal
# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __int__(self, categoty_name='base'):
        self.name = categoty_name

    def __str__(self):
        return self.name

class Shop(models.Model):
    name = models.CharField(max_length=120, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=False)
    purchase_price = models.FloatField(default=5)
    pharma_price = models.FloatField(default = 6)
    quantity = models.IntegerField(default = 10)
    image = models.TextField(null=True)
    # created = models.DateTimeField(auto_now_add=True)
    # updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
        
    def get_absolut_url(self):
        return "/view/%s" % self.pk
