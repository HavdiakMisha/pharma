from django import forms
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Shop
from .models import Category
import os
import base64
from io import StringIO



class OrderFromStorage(forms.ModelForm):
    name = forms.CharField(max_length=120)
    category = Category(forms.CharField(max_length=120))
    purchase_price = forms.FloatField()
    pharma_price = forms.FloatField()
    quantity = forms.IntegerField()

    # image_2 = forms.ImageField()
    # img_buffer = StringIO()

    # image.save(img_buffer, format="jpg")
    # image = base64.b64encode(img_buffer.getvalue())
    # with open("yourfile.ext", "rb") as image_file:
    class Meta:
        model = Shop
        # fields = ['name','category', 'purchase_price', 'pharma_price', 'quantity', 'image']
        fields = ['name','category', 'purchase_price', 'pharma_price', 'quantity']



