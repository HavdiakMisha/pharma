from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='home_about'),
    path('thank-you', views.thank_u, name='home_thank_you'),
]