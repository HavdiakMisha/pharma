from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm
from django.contrib.auth import logout


def registration(request):
    if request.method == 'POST':
        # form = UserRegisterForm(request.POST)
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f' {username} дякуємо, ми раді, що ви обрали наш сервіс!')
            return redirect('home_thank_you')
    else:
        # form = UserRegisterForm()
        form = UserCreationForm()
    return render(request, 'users/register.html', {'form':form })



def logout_view(request):
    logout(request)
    return redirect('home')